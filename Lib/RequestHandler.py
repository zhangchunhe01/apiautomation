import requests
import json
from Config.Conf import config as conf
class RequestHandler:
    def __init__(self):
        """session管理器"""
        self.session = requests.session()

    def send_request(self, method, url=None, params=None, data=None, json=None, headers=None, **kwargs):
        if method == "get":
            headers = {"token": conf.token}
            response = self.session.get(url=url, params=params, headers=headers, verify=True, **kwargs)
            return response.json()
        if method == "post":
            headers = {"token": conf.token,
                       'content-type': 'application/json',
                       "charset":"UTF-8"}
            response = self.session.post(url=url,data=data, json=data,headers=headers, verify=True, **kwargs)
            return response.json()
        if method == "put":
            headers = {"token": conf.token,
                       'content-type': 'application/json'}
            response = self.session.put(url=url, data=data, headers=headers, verify=True, **kwargs)
            return response.json()
        if method == "delete":
            headers = {"token": conf.token,
                       'content-type': 'application/json'}
            response = self.session.delete(url=url, headers=headers, verify=True, **kwargs)
            return response.json()

    def close_session(self):
        """关闭session"""


if __name__ == '__main__':
    RequestHandler().send_request(method="get", url="https://api-pp.backoffice.allforsport.cn/v1/event_templates",
                                  params={"status": "0", "current_page": "1", "store_id": "15"})
