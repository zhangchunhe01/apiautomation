import pytest
import requests, json
from Lib.RequestHandler import RequestHandler
from Tools.GetExcel import get_excel
from Config.Conf import config as conf


@pytest.fixture()
def add_template():
    data = {
        "classPackes": [],
        "address": "上海市青浦区沪青平公路1821号(沪青平公路近A5嘉金高速公路，永业购物中心一层)",
        "allowRegisteration": 0,
        "banner": "https://platformpp.blob.core.chinacloudapi.cn/platformpp/QR_202103151645048308.jpeg",
        "cancelHour": 0,
        "freeCancel": "false",
        "cover": "https://platformpp.blob.core.chinacloudapi.cn/platformpp/QR_202103151645048308.jpeg",
        "description": "活动须知",
        "duration": 30,
        "level": "高阶",
        "ownerId": "",
        "registerFee": "",
        "registerMax": "4",
        "registerMin": 0,
        "registerType": "0",
        "targetUsers": ["成人", "亲子"],
        "title": "自动化测试徐泾活动01(fixture)",
        "classType": "1",
        "eventDesctiption": "活动介绍",
        "note": "",
        "sportName": "",
        "freeCancelTime": 0,
        "pay": "",
        "gps": "121.2819,31.1688",
        "sportsType": 88,
        "allowRegistration": 4,
        "storeId": 15,
        "eventDescription": "活动介绍"
    }
    func = RequestHandler()
    urlPath = "https://api-pp.backoffice.allforsport.cn/v1/event_templates"
    resp = func.send_request(method="post", url=urlPath, data=json.dumps(data))
    return resp['result']['id']

if __name__ == '__main__':
    pytest.main(["conftest.py","-v", "-s"])
