import pytest
import sys

sys.path.append("")
import allure

from Config.Conf import config as conf
import json
from Tools.GetExcel import get_excel
from Tools.Log import MyLog
from Lib.RequestHandler import RequestHandler
from Tools.GetExcel import get_excel
import requests

@allure.epic("Booking_自动化测试项目")
@allure.feature("活动列表模块")
class Test_events():
    @allure.story("添加活动场次")
    @allure.title("添加活动场次用例")
    @pytest.mark.parametrize("url,inData,exceptData", get_excel("events", "Add"))
    def test_add_event(self,url, inData, exceptData,add_template):
        func = RequestHandler()
        urlPath = conf.url+url

        inData[0]["eventTemplateId"] = add_template
        '''读取Excel用例中参数为单引号，转化为json双引号'''
        resp = func.send_request(method="post",url=urlPath,data=json.dumps(inData))
        print(resp)
        MyLog.info(f"请求路径为:{urlPath}")
        MyLog.info(f"请求参数为:{inData}")
        MyLog.info(f"预期结果为:{exceptData}")
        MyLog.info(f"实际响应结果为:{resp}")
        try:
            assert resp['resultDesc'] == exceptData['resultDesc']
        except Exception as e:
            MyLog.error(f"添加活动列表异常，原因:{e}")




    # @pytest.mark.parametrize("url,inData,exceptData", get_excel("events", "Query"))
    # @allure.story("查询所有活动列表")
    # @allure.title("查询活动列表用例")
    # def test_query_events(self, url, inData, exceptData):
    #     func = RequestHandler()
    #     urlPath = conf.url + url
    #     resp = func.send_request(method="get", url=urlPath, params=inData)
    #     MyLog.info(f"请求参数为:{inData}")
    #     MyLog.info(f"预期结果为:{exceptData}")
    #     MyLog.info(f"实际响应结果为:{resp}")
    #     try:
    #         assert resp['resultDesc'] == exceptData['resultDesc']
    #     except Exception as e:
    #         MyLog.error(f"查询活动列表异常，原因:{e}")


if __name__ == '__main__':
    pytest.main(["-v","-s","-q"])
