import pytest
import sys

sys.path.append("")
import allure
from Config.Conf import config as conf
import json
from Tools.GetExcel import get_excel
from Tools.Log import MyLog
from Lib.RequestHandler import RequestHandler


@allure.epic("Booking_自动化测试项目")
@allure.feature("活动模板模块")
class Test_event_template():
    # 初始默认值
    templateId = 0

    @allure.story("创建活动模板")
    @allure.title("创建活动模板用例")
    @pytest.mark.parametrize("url,inData,exceptData", get_excel("templates", "Add"))
    def test_add_template(self, url, inData, exceptData):
        '''调用send_request'''
        try:
            func = RequestHandler()
            urlPath = conf.url + url
            resp = func.send_request(method="post", url=urlPath, data=json.dumps(inData))
            MyLog.info(f"请求地址：{urlPath}")
            MyLog.info(f"请求参数：{inData}")
            MyLog.info(f"预期结果:{exceptData}")
            MyLog.info(f"实际响应结果:{resp}")
            assert resp['resultDesc'] == exceptData['resultDesc']
            global templateId
            templateId = resp['result']['id']
        except Exception as e:
            MyLog.error(f"创建活动模板异常，原因:{e}")

    @allure.story("修改活动模板")
    @allure.title("修改活动模板用例")
    @pytest.mark.parametrize("url,inData,exceptData", get_excel("templates", "Modify"))
    def test_modify_template(self, url, inData, exceptData):
        '''调用send_request'''
        try:
            global templateId
            addToRequestBody = {"id": templateId}
            mergeData = {**addToRequestBody, **inData}
            func = RequestHandler()
            urlPath = conf.url + url
            resp = func.send_request(method="put", url=urlPath, data=json.dumps(mergeData))
            MyLog.info(f"请求地址：{urlPath}")
            MyLog.info(f"请求参数：{mergeData}")
            MyLog.info(f"预期结果:{exceptData}")
            MyLog.info(f"实际响应结果:{resp}")
            assert resp['resultDesc'] == exceptData['resultDesc']
        except Exception as e:
            MyLog.error(f"修改活动模板异常，原因:{e}")

    @allure.story("查询所有活动模板")
    @allure.title("查询活动模板用例")
    @pytest.mark.parametrize("url,inData,exceptData", get_excel("templates", "Query"))
    def test_query_template(self, url, inData, exceptData):
        """调用send_request"""
        try:
            func = RequestHandler()
            urlPath = conf.url + url
            resp = func.send_request(method="get", url=urlPath, params=inData)
            MyLog.info(f"请求地址：{urlPath}")
            MyLog.info(f"请求参数：{inData}")
            MyLog.info(f"预期结果:{exceptData}")
            MyLog.info(f"实际响应结果:{resp}")
            assert resp['resultDesc'] == exceptData['resultDesc']
            print(resp)
        except Exception as e:
            MyLog.error(f"查询活动模板异常，原因:{e}")

    @pytest.mark.parametrize("url,inData,exceptData", get_excel("templates", "Delete"))
    @allure.story("删除活动模板")
    @allure.title("删除活动模板用例")
    def test_delete_template(self, url, inData, exceptData):
        """调用send_request"""
        try:
            global templateId
            func = RequestHandler()
            urlPath = conf.url + url
            resp = func.send_request("delete", url=urlPath + "/" + str(templateId))
            MyLog.info(f"请求地址：{urlPath}")
            MyLog.info(f"请求参数：{inData}")
            MyLog.info(f"预期结果:{exceptData}")
            MyLog.info(f"实际响应结果:{resp}")
            assert resp['resultDesc'] == exceptData['resultDesc']
        except Exception as e:
            MyLog.error(f"删除活动模板异常，原因:{e}")


if __name__ == '__main__':
    pytest.main(["-v"])
