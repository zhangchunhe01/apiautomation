import xlrd,json


def get_excel(sheetName,caseName):
    listBody = []
    index = 0
    workBook = xlrd.open_workbook("../Data/TestCase.xls",formatting_info=True)
    workSheet = workBook.sheet_by_name(sheetName)
    for one in workSheet.col_values(0):
        if caseName in one:
            requestUrl = workSheet.cell(index,5).value
            requestBody = workSheet.cell(index,8).value
            exceptBody = workSheet.cell(index,9).value
            listBody.append((requestUrl,json.loads(requestBody),json.loads(exceptBody)))
        index = index+1
    print(listBody)
    return listBody
if __name__ == '__main__':
    get_excel("setup", "fixture_add_template")