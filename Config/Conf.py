import os


class Config(object):  # 默认配置
    DEBUG = False

    def __getitem__(self, key):
        return self.__getattribute__(key)


class DevelopmentConfig(Config):
    url = "https://api-dev.backoffice.allforsport.cn"
    file_path = '../Data/test_case.xls'


class PreProductionConfig(object):
    url = "https://api-pp.backoffice.allforsport.cn"
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzSW4iOjE2MTY3MjE5MDY4NjAsImlkIjoyMDB9.kjQS5NmN7nDkDiqKz5mtBFDRgl2WeuleI_0_NNPGV_s"


# 环境映射关系
mapping = {
    'dev': DevelopmentConfig,
    'pp': PreProductionConfig
}
# 一键切换环境
APP_ENV = os.environ.get('APP_ENV', 'pp').lower()
config = mapping[APP_ENV]()  # 获取指定的环境
