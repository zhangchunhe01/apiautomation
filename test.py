import requests
import json
'''一键取消所有活动场次'''
def delete_all_events():

    for eventId in range(147800, 150000):
        url = "https://api-pp.backoffice.allforsport.cn/v1/events/" + str(eventId)
        headers = {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzSW4iOjE2MTY3MjE5MDY4NjAsImlkIjoyMDB9.kjQS5NmN7nDkDiqKz5mtBFDRgl2WeuleI_0_NNPGV_s",
            'content-type': 'application/json'}
        body = {"description": "1234",
                "id": eventId,
                "megSendParticipants": "",
                "reasonType": 2}
        resp = requests.delete(url=url, headers=headers, json=body, verify=True)

        print(resp.json())
def request_test():
    url = "https://api-pp.backoffice.allforsport.cn/v1/events/"
    headers = {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzSW4iOjE2MTY3MjE5MDY4NjAsImlkIjoyMDB9.kjQS5NmN7nDkDiqKz5mtBFDRgl2WeuleI_0_NNPGV_s",
        'content-type': 'application/json'}
    body = [{
	"rangeTime": ["2021-03-31 00:00:00", "2021-03-31 00:30:00"],
	"startTime": "2021-03-31 00:00:00",
	"endTime": "2021-03-31 00:30:00",
	"published": "false",
	"availableDate": "",
	"coachId": "",
	"minRegister": 0,
	"maxRegister": 4,
	"description": "",
	"address": "上海市青浦区沪青平公路1821号(沪青平公路近A5嘉金高速公路，永业购物中心一层)",
	"latLng": {
		"lng": 121.2819,
		"lat": 31.1688
	},
	"gps": "121.2819,31.1688",
	"note": "",
	"storeId": 15,
    "eventTemplateId":186616
}]
    print(type(body))
    resp = requests.post(url=url,json=body,headers=headers,verify=True)
    print(resp.json())
if __name__ == '__main__':
    request_test()